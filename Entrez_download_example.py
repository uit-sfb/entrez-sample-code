from Bio import Entrez
import urllib, os
Entrez.email = 'espen.m.robertsen@uit.no'

# Api key support requires newer versions of Biopython (tested with 1.78)
#Entrez.api_key = 'api_key'

# Two random MarDB accessions, one Assembly (GCA) one from Refseq (GCF, which should be in both Assembly and Refseq GCA/GCF)
accession_list = ['GCA_002749115.1', 'GCF_000953735.1']

# Search assembly db using each accession.
for accession in accession_list:
    handle = Entrez.esearch(db="assembly", term=accession)
    record = Entrez.read(handle)
    # Check that each unique accession has only on unique id. This part should probably be wrapped in a try / except instead, with some additional logic/logging
    if len(record['IdList']) == 1:
        print("Only one id found, continuing")
        # Extract "id" (Every accession should have this, i guess)
        id = record['IdList']
        # Use esumary to retrieve report full, which has ftp-paths hidden deep inside the insanity
        esummary = Entrez.esummary(db="assembly", id=id, report="full")
        record = Entrez.read(esummary)
        gca_path = record['DocumentSummarySet']['DocumentSummary'][0]['FtpPath_GenBank']
        gcf_path = record['DocumentSummarySet']['DocumentSummary'][0]['FtpPath_RefSeq']

        # If a GCF is provided, both of these should be defined. If GCA, only gca_path
        print (gcf_path)
        print (gca_path)

        ### Implement wget / curl here, using f.ex the subprocess library, or requests or whatever you like to download with
        ### Example using urllib.request with gca_path:

        # The gca ftp path from Entrez i missing some parts, which we need to build. Example under:
        # gca_path - ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/953/735/GCA_000953735.1_MVIS1
        # "Real" ftp path to fasta file - ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/953/735/GCA_000953735.1_MVIS1/GCA_000953735.1_MVIS1_genomic.fna.gz
        postfix = os.path.split(urllib.parse.urlsplit(gca_path).path)[-1]
        ftp_path = urllib.parse.urljoin(gca_path+"/", postfix)

        # These suffixes are generic and can be found in any ftp listing.
        ftp_path = "{}_genomic.fna.gz".format(ftp_path)

        # Retrieve using urllib
        data = urllib.request.urlretrieve(ftp_path, os.path.split(urllib.parse.urlsplit(ftp_path).path)[-1])

    else:
        exit("No ids or multiple ids for one accession found - Needs implementation of adhoc code")
